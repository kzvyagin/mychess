#ifndef CHESSUNITPROVIDER_H
#define CHESSUNITPROVIDER_H

#include <QQuickImageProvider>
#include <QPixmap>
#include <QDebug>

class ChessUnitProvider : public QQuickImageProvider
{
    QPixmap puzzleImage;
    QString currentPixmap;
    int totalUnitsCount;
    int totalRows;
    int whiteRow;
    int blackRow;
    QSize oneUnitSize;
    QHash <QString,QRect> unitsMap;
public:

    ChessUnitProvider();

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // CHESSUNITPROVIDER_H
