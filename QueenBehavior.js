Qt.include("qrc:/Units/BehaviorPrtotype.js")
var QueenBehavior = function ( ) {
    var that = BehaviorPrototype( );
    that.isAccesebleMove = function(model ,  oldIndex , newIndex){
        console.log("QueenBehavior.isAccesebleMove",model,oldIndex,newIndex)   ;
        var currentElement=model.get(oldIndex);
        if( !model || !oldIndex || !newIndex || !currentElement )
            return;

        var dioganal=that.isDioganalLine(model,oldIndex,newIndex);
        console.log( "queen.isDioganalLine=", dioganal);
        if(dioganal){
            var bloked=that.isDioganalLineBlocked(model,oldIndex,newIndex)
            console.log( "queen.isDioganalLineBlocked=",bloked)
            if(bloked)
                return "no"

            if(that.isElementIsEmpy(model,newIndex) )
                return "yes"

            if(    !that.isElementIsEmpy(model,newIndex)
                && that.isEatable(model,oldIndex,newIndex) )
                return "eatable";
        }

        var stright=that.isStrightLine(model,oldIndex,newIndex);
        console.log( "queen.isStrightLine=", stright);

        if(stright)
        {
            var bloked=that.isStrightLineBlocked(model,oldIndex,newIndex)
            if(bloked){
                return "no"
            }

            if(that.isElementIsEmpy(model,newIndex) ){
                return "yes"
            }

            if(    !that.isElementIsEmpy(model,newIndex)
                && that.isEatable(model,oldIndex,newIndex) ){
                return "eatable";
            }
        }
        return "no";
    }
    return that;
}
