#ifndef TURNMANAGER_H
#define TURNMANAGER_H

#include <QObject>
#include <QVariant>
#include "turnsstory.h"
#include <QVariantMap>
#include <QDebug>
class TurnManager : public QObject
{
    Q_OBJECT
public:
    explicit TurnManager(QObject *parent = 0);

signals:
    void turnChanged(const QString &turnVal);
public slots:
    void turnHappened(QVariant event,
                      QVariantMap item,
                      QVariant newPos,
                      QVariant fromPos,
                      QVariantMap lendItem);

    QString whoseMove();
    void saveCurrentHistory(QVariant fileName);
    void loadHistory(QVariant fileName);
    void clearHistory();
    int getToltalTurnsCount();
    QVariantMap getTurn(QVariant _number);

private:
    QString currentTurn;

};

#endif // TURNMANAGER_H
