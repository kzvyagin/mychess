#ifndef REFBOOK_H
#define REFBOOK_H

#include <QObject>
#include <QHash>
#include <QVariantMap>
class RefBook : public QObject
{
    Q_OBJECT
public:
    explicit RefBook(QObject *parent = 0);

signals:

public slots:
    Q_INVOKABLE const QHash<int,QString> & getStartSet();
    Q_INVOKABLE QStringList getFigureNameFromStartSet(QVariant index);

};

#endif // REFBOOK_H
