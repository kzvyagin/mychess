Qt.include("qrc:/Utils.js")

var BehaviorPrototype = function( ) {
    var that = {};
    that.boardSize=8;

    that.isAccesebleMove = function(model ,  oldIndex , newIndex){
        console.log("BehaviorPrototype.isAccesebleMove",model,oldIndex,newIndex)   ;
        return "no";
    };


    that.isElementIsEmpy=function(model ,  index){
        if(index<0)
            return false;
        return model.get(index).iName.length===0;
    };

    that.isEatable=function(model,oldIndex,newIndex){
        if(oldIndex<0 || newIndex <0)
            return false;
        var attacer= model.get(oldIndex);
        var defencer= model.get(newIndex);
        return attacer.iColor!==defencer.iColor;
    };

    that.touchElement=function( element ){
        console.log(" BehaviorPrototype touchElement",element.iTouched)
        element.iTouched=true;

    }




    that.isDioganalLine =function(model,oldIndex,newIndex){
        if(oldIndex<0 || newIndex <0  )
            return false;
        var oldRowNumber   =parseInt(oldIndex/8);
        var newRowNumber=parseInt(newIndex/8);

        var startCol= (oldIndex%8);
        var endCol= (newIndex%8);


        console.log("isDioganalLine","oldIndex",oldIndex,"newIndex",newIndex)
        console.log("isDioganalLine","oldColumnNumber",oldRowNumber,"newColumnNumber",newRowNumber)
        console.log("isDioganalLine","startCol",startCol,"endCol",endCol)

        console.log("isDioganalLine","IS DIOGANAL",Math.abs(startCol-endCol)===Math.abs(oldRowNumber-newRowNumber))


        if(Math.abs(startCol-endCol)===Math.abs(oldRowNumber-newRowNumber)){
            return true;
        }

        return false;
    }

    that.isDioganalLineBlocked =function(model,oldIndex,newIndex){

        var diagonalElements=that.getDioganalElements(model,oldIndex,newIndex);
        console.log("isDioganalLineBlocked diagonalElements.length=",diagonalElements.length);
        if(diagonalElements.length<=0)
            return false;
        for(var i=0;i<diagonalElements.length;i++){
            console.log("diagonalElements[i].iName=",diagonalElements[i].iName,diagonalElements[i].iName.length,diagonalElements[i].iName.length>0);
            if( diagonalElements[i].iName.length > 0 ){
                //console.log("BLOCKED!!!");
                return true;
            }
        }
        return false;
    }


    that.getDioganalElements=function (model,oldIndex,newIndex){
        if(oldIndex<0 || newIndex <0  )
            return [];

        var oldRowNumber   =parseInt(oldIndex/8);
        var newRowNumber   =parseInt(newIndex/8);

        var startCol= (oldIndex%8);
        var endCol= (newIndex%8);

        var signCol=1;
        var colMatch=function(a,b){
            return a>b;
        }
        if( startCol > endCol ){
              signCol=-1;
              colMatch=function(a,b){
                return a<b;
            }

        }

        var signRow=1;
        var rowMatch=function(a,b){
            return a>b;
        }
        if( oldRowNumber > newRowNumber ){
              signRow=-1;
              rowMatch=function(a,b){
                return a<b;
            }
        }
        var result=[];
        console.log("getDioganalElements","oldIndex",oldIndex,"newIndex",newIndex)
        console.log("getDioganalElements","oldColumnNumber",oldRowNumber,"newColumnNumber",newRowNumber)
        console.log("getDioganalElements","startCol",startCol,"endCol",endCol)

        for( var i=startCol+1*signCol,
                j=oldRowNumber+1*signRow,k=0;
             colMatch(endCol,i) && rowMatch(newRowNumber,j);
             i+=signCol,j+=signRow ){
                    var index=i+j*8;
                    console.log("getDioganalElements index=",index)
                    if(index<0 || index>=model.cout)
                        continue;
                    result[k++]=model.get(index);
        }
        return result;

    }



    that.isStrightLine=function(model,oldIndex,newIndex) {
        if(oldIndex<0 || newIndex <0)
            return false;
        var rowNumber=parseInt(oldIndex/8);
        var diff = newIndex-oldIndex  ;

        if(    Math.abs(diff) < 8
                && parseInt( (oldIndex+diff)/8 ) === rowNumber )
        {
            return true;
        }
        var line=Math.abs( (oldIndex-newIndex)/8 );
        if( parseInt(line)== line ){
            return true;
        }

        return false;
    }
    that.isStrightLineBlocked =function(model,oldIndex,newIndex){
        var strightLineElements=that.getStrightLineElements(model,oldIndex,newIndex);
        console.log("isStrightLineBlocked strightLineElements.length=",strightLineElements.length);
        if(strightLineElements.length<=0)
            return false;
        for(var i=0;i<strightLineElements.length;i++){
            console.log("strightLineElements[i].iName=",strightLineElements[i].iName,strightLineElements[i].iName.length,strightLineElements[i].iName.length>0);
            if( strightLineElements[i].iName.length > 0 ){
                //console.log("BLOCKED!!!");
                return true;
            }
        }
        return false;
    }
    that.getStrightLineElements=function (model,oldIndex,newIndex){
        if(oldIndex<0 || newIndex <0)
            return [];

        var oldRowNumber=parseInt(oldIndex/8);
        var newRowNumber=parseInt(newIndex/8);
        var diff = newIndex-oldIndex  ;
        var result=[];
        console.log("getStrightLineElements",oldIndex,newIndex,oldRowNumber,newRowNumber);
        if( oldRowNumber===newRowNumber ) //same line
        {
            var startIndex= oldIndex >newIndex ? newIndex+1 : oldIndex+1;
            var endIndex  = oldIndex >newIndex ? oldIndex : newIndex;
            console.log("getStrightLineElements",startIndex,endIndex);
            for( var index=startIndex,i=0; index<endIndex; index++,i++ ){
                if( index<0 || model >=model.count ){
                    --i;
                    continue;
                }
                console.log("getStrightLineElements",index);
                result[i]=model.get(index);
            }
            return result;
        }

        var startLine = oldRowNumber>newRowNumber ? newRowNumber+1 : oldRowNumber+1;
        var endLine   = oldRowNumber>newRowNumber ? oldRowNumber : newRowNumber;
        console.log("getStrightLineElements","columnNewNumber=",newRowNumber,"columnNumber=",oldRowNumber);
        console.log("getStrightLineElements","startLine=",startLine,"endLine=",endLine);

        for(var index=startLine,i=0;index<endLine;index++,i++ ){
            if( index<0 || model >=model.count ){
                --i;
                continue;
            }
            console.log("getStrightLineElements",index,oldIndex+(index-oldRowNumber)*8);
            result[i]=model.get( oldIndex+(index-oldRowNumber)*8 );
        }

        return result;

    }




    that.isCastlingAvailible=function (model,oldIndex,newIndex){
        var pointElement=model.get(newIndex);
        var souceElement=model.get(oldIndex);

        console.log("rokerovka",    pointElement.iTouched===false
                    , souceElement.iTouched===false
                    , pointElement.iColor===souceElement.iColor
                    , (    souceElement.iName==="king" && pointElement.iName==="turret"
                       || souceElement.iName==="turret" && pointElement.iName==="king" )
                    , that.isStrightLine(model ,  oldIndex , newIndex)
                    , !that.isStrightLineBlocked(model ,  oldIndex , newIndex) );
        if(        pointElement.iTouched===false
                && souceElement.iTouched===false
                && (    souceElement.iName==="king" && pointElement.iName==="turret"
                    || souceElement.iName==="turret" && pointElement.iName==="king" )
                && pointElement.iColor===souceElement.iColor
                && that.isStrightLine(model ,  oldIndex , newIndex)
                && !that.isStrightLineBlocked(model ,  oldIndex , newIndex)
                && ( Math.abs(oldIndex-newIndex) === 3 || Math.abs(oldIndex-newIndex) === 4 ))
        {
            return true;
        }
        return false;
    }

    that.getCastlingIndexes=function (model,oldIndex,newIndex){

        var kingPoint=0,turretPoint=0;
        if(model.get(oldIndex).iName==="king"){
            kingPoint=oldIndex;
            turretPoint=newIndex;
        }
        else {
            kingPoint=newIndex;
            turretPoint=oldIndex;
        }

        if( Math.abs(oldIndex-newIndex) === 3)
        {
            kingPoint-=2;
            turretPoint+=2;

        }else
            if( Math.abs(oldIndex-newIndex) === 4){
                kingPoint+=2;
                turretPoint-=3;

            }

        return{"king": kingPoint ,"turret": turretPoint};
    }
    return that;
}


