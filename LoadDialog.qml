import QtQuick 2.2
import QtQuick.Dialogs 1.0

FileDialog {
    id: fileDialog
    title: "Please choose a file"
    nameFilters: [ "Chess game files (*.chf)"]
    selectExisting: true
    signal userSelectFile
    onAccepted: {
        console.log("You chose: " + fileDialog.fileUrl )
        TurnManager.loadHistory(fileDialog.fileUrl);
        userSelectFile();

    }
    onRejected: {
        console.log("Canceled")

    }
    Component.onCompleted: visible = true
}
