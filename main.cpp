#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "utils.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    Utils::registerPixmapProvider(&engine);
    Utils::registerStuffObjects(&engine);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
