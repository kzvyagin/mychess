import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "CommonWidgets.js" as Widgets
import "Utils.js" as Utils
import "CellItem.js" as CellItem
import "MovesLogic.js" as MovesLogic
import QtQuick.Dialogs 1.0
Rectangle {

    id: chessBoard
    color: "#222222"
    anchors.fill: parent
    property bool chessReadOnly: false
    property bool gameOver: false
    signal gameOvered
    signal boardStatus( string status )
    onGameOverChanged: {
        gameOvered() ;
    }
    MouseArea{
        anchors.fill: parent
    }
    ListModel {
        id: cellModel
    }
    Component.onCompleted: {
        fillDataToModelByDefault()
        console.log("chessBoard is created")

    }


    function fillDataToModelByDefault(){
        cellModel.clear();
        for(var i=0;i<64;i++){
            var params=RefBook.getFigureNameFromStartSet(i);

            cellModel.append( CellItem.crete(   (params.length>1 ? params[1]: "")
                                             ,(params.length>1  ? params[0]: "")
                                             ,i ))
        }
    }


    Component {
        id: widgetdelegate
        Item {
            width: grid.cellWidth;
            height: grid.cellHeight

            Item {
                id: im
                state: "inactive"
                width: grid.cellWidth - 10;
                height: grid.cellHeight - 10

                //                Text {
                //                    id: name
                //                    text: iNumber
                //                }
                Image {
                    id: rrrr
                    anchors.fill: parent
                    source:   iName.length>0 ? ("image://ChessUnit/chess-figures="+iName+" "+iColor) : ""
                }

                states: [
                    State {
                        name: "inactive";
                        when: (grid.firstIndexDrag == -1) || (grid.firstIndexDrag == index)

                    }
                ]
            }

            states: [
                State {
                    name: "inDrag"
                    when: index == grid.firstIndexDrag

                    PropertyChanges { target: im; parent: container }
                    PropertyChanges { target: im; anchors.centerIn: undefined }
                    PropertyChanges { target: im; x: coords.mouseX - im.width/2 }
                    PropertyChanges { target: im; y: coords.mouseY - im.height/2 }
                }
            ]
        }
    }
    Row
    {
        anchors.fill: parent
        spacing: 5

    Column{
        width:   parent.width-20
        height:  parent.width-10
        spacing: 10


        Item {
            id: boardItem
            height: parent.width>parent.height ? parent.height-10  : parent.width-10
            width: parent.width>parent.height ? parent.height  : parent.width

            Image {
                id: name
                anchors.fill: parent
                source: "qrc:/images/images/chessBoard.jpeg"
            }
            GridView {

                property int firstIndexDrag: -1
                id: grid
                property int rowElementsCount: 8
                property int colElementsCount: 8
                interactive: false // no flickable
                anchors.fill: parent
                cellWidth: parent.width / rowElementsCount;
                cellHeight: parent.height /colElementsCount;

                model: cellModel
                delegate: widgetdelegate

                Item {
                    id: container
                    anchors.fill: parent
                }

                MouseArea {
                    id: coords
                    anchors.fill: parent
                    onReleased:  {
                        var newIndex = grid.indexAt(mouseX, mouseY);
                        var oldIndex = grid.firstIndexDrag;
                        makeMove(oldIndex,newIndex,true)
                    }
                    onPressed: itemPressed(mouseX,mouseY)
                }
            }

        }// GridView Item
        Item {
            width: parent.width
            height: 10;
            Row{
                width: parent.width
                height: 10;
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("a")}
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("b")}
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("c")}
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("d")}
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("e")}
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("f")}
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("g")}
                Text { width:parent.width/8; color: "white"; horizontalAlignment: TextInput.AlignHCenter; text: qsTr("h")}
            }
        }

    }//Collum
    Item {
        width: 10
        height: parent.height;
        Column{
            width: 10
            height: parent.height;
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("8")}
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("7")}
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("6")}
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("5")}
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("4")}
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("3")}
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("2")}
            Text { height:parent.height/8; color: "white"; verticalAlignment: TextInput.AlignHCenter; text: qsTr("1")}
        }
    }
    }//Row

    function itemPressed(mouseX, mouseY){
        if(chessReadOnly || gameOver)
            return;

        var turnColor=TurnManager.whoseMove();
        console.log("TurnManager.turnColor=",turnColor)
        var index=grid.indexAt(mouseX, mouseY)
        var element=cellModel.get(index)
        if(element.iName.length===0 || element.iColor !== turnColor){
            return;
        }


        grid.firstIndexDrag = grid.indexAt(mouseX, mouseY)
    }

    function makeMove( oldIndex, newIndex , manageTurn) {

        if( newIndex <0 || oldIndex<0 || newIndex===oldIndex){
            grid.firstIndexDrag = -1;
            return;
        }


        var dragElement=cellModel.get(oldIndex);
        var element=cellModel.get(newIndex);
        var dragCopy= CellItem.clone( dragElement )
        var elementCopy= CellItem.clone( element )

        if( !dragElement || !element ){
            grid.firstIndexDrag = -1;
            return;
        }

        var bechavior=MovesLogic.unitBehavior();

        if( Object.keys(bechavior).indexOf(dragElement.iName) ===-1){
            grid.firstIndexDrag = -1;
            return;
        }

        var result=bechavior[ dragElement.iName ].isAccesebleMove( cellModel ,oldIndex,newIndex)
        console.log("bechavior result=",result," by name=",dragElement.iName);


        switch(result){
        case "yes":

            bechavior[ dragElement.iName ].touchElement(dragCopy);
            cellModel.set(oldIndex,elementCopy)
            cellModel.set(newIndex,dragCopy)
            grid.firstIndexDrag = -1

            break;
        case "no":cellModel.set(oldIndex,dragElement);
            grid.firstIndexDrag = -1;
            return;
        case "eatable":

            bechavior[ dragElement.iName ].touchElement(dragCopy);
            cellModel.set(oldIndex,{ iNumber: elementCopy.iNumber ,iName: "" ,iColor:  ""  })
            cellModel.set(newIndex,dragCopy)


            grid.firstIndexDrag = -1

            break;

        case "castling":

            var indexes=bechavior[ dragElement.iName ].getCastlingIndexes( cellModel ,oldIndex,newIndex,CellItem.clone(cellModel.get( newIndex )));

            if( dragCopy.iName==="king" ){
                cellModel.set(indexes.turret,elementCopy)
                cellModel.set(indexes.king,dragCopy)
                cellModel.set(oldIndex, CellItem.crete("","",indexes.king)   )
                cellModel.set(newIndex, CellItem.crete("","",indexes.turret) )
            }else{
                cellModel.set(indexes.king,elementCopy)
                cellModel.set(indexes.turret,dragCopy)
                cellModel.set(oldIndex, CellItem.crete("","",indexes.turret))
                cellModel.set(newIndex, CellItem.crete("","",indexes.king))
            }

            bechavior[ "king" ].touchElement(elementCopy);
            bechavior[ "king" ].touchElement(dragCopy);
            grid.firstIndexDrag = -1;
            break;
        case "pawnTransform":

            bechavior[ dragElement.iName ].touchElement(dragElement);
            cellModel.set(oldIndex,{ iNumber: elementCopy.iNumber ,iName: "" ,iColor:  ""  })
            dragCopy.iName="queen";
            cellModel.set(newIndex,dragCopy)


            grid.firstIndexDrag = -1
            break;
        default:
            grid.firstIndexDrag = -1;
            return;
        }

        if(manageTurn)
        {
            TurnManager.turnHappened(result,dragCopy,newIndex,oldIndex,elementCopy ) ;
        }
        checkForGameOver( chessReadOnly===false ? TurnManager.whoseMove() : (dragCopy.iColor=="white" ? "black":"white"));

    }

    function makeReverceMove( move  )
    {
        var oldIndex=move.from;
        var newIndex=move.to;
        var figureEatedColor=move.ecolor;
        var figureEatedName=move.ename;
        var  event=move.event;
        if( event==="castling" ){
            var turret = CellItem.crete(figureEatedColor,"turret",newIndex);
            var king   = CellItem.crete(figureEatedColor,"king",oldIndex);
            cellModel.set(oldIndex, figureEatedName==="king"?turret :king);
            cellModel.set(newIndex, figureEatedName==="king"?king :turret);
            var bechavior=MovesLogic.unitBehavior();
            var elements=bechavior["king"].getStrightLineElements(cellModel,oldIndex,newIndex);
            for(var i=0;i< elements.length;i++){
                elements[i].iColor="";
                elements[i].iName="";
            }
            grid.firstIndexDrag = -1;
            checkForGameOver( figureEatedColor=="white" ? "black":"white");

            return;
        }else if( event==="pawnTransform")
        {
            var currentElement=cellModel.get(newIndex);
            var newFigure   = CellItem.crete(figureEatedColor,figureEatedName,newIndex);
            var pawnFigure   = CellItem.crete(currentElement.iColor,"pawn",oldIndex);
            cellModel.set(newIndex, newFigure);
            cellModel.set(oldIndex, pawnFigure);
            grid.firstIndexDrag = -1;
            checkForGameOver( currentElement.iColor=="white" ? "black":"white");

            return;
        }
        var dragElement=cellModel.get(newIndex);
//        console.log("makeReverceMove dragElement=",JSON.stringify(dragElement)
//                    ,"figureEatedColor=",figureEatedColor,
//                    "figureEatedName=", figureEatedName)

        cellModel.set(oldIndex, CellItem.clone(dragElement))
        cellModel.set(newIndex, CellItem.crete(figureEatedColor,figureEatedName,newIndex))
        cellModel.get(oldIndex).iTouched=false;
        cellModel.get(newIndex).iTouched=false;
        grid.firstIndexDrag = -1;
        checkForGameOver(   dragElement.iColor=="white" ? "black":"white");


    }

    function checkForGameOver( currentColor ){
            var allKings= MovesLogic.getAllKings( cellModel );
            console.log("checkForGameOver","allKings",JSON.stringify(allKings));
            if(allKings.length!==2){
                chessBoard.gameOver=true;
                return;

            }
            var kings=MovesLogic.getAllKingsUnderAttack(cellModel);
            console.log("checkForGameOver","kings",JSON.stringify(kings));
            console.log("checkForGameOver","currentColor",currentColor);
            if(kings.length===0){
                boardStatus("");
                return;
            }
            boardStatus("CHECK");

            for(var i=0;i<kings.length;i++){
                console.log("checkForGameOver","kings.attacked",kings[i].attacked,kings[i].iColor,currentColor);
                if(kings[i].attacked && kings[i].iColor!==currentColor){
                    chessBoard.gameOver=true;
                    boardStatus("CHECKMATE !!!");
                    return;
                }
            }
    }
}

