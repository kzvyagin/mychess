#ifndef UTILS_H
#define UTILS_H


#include <QQmlApplicationEngine>

class Utils
{
public:
    Utils();
    static void registerPixmapProvider(QQmlApplicationEngine *viewer);
    static void registerStuffObjects(QQmlApplicationEngine *viewer);

};

#endif // UTILS_H
