import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "CommonWidgets.js" as Widgets
import "Utils.js" as Utils
import "CellItem.js" as CellItem
import "MovesLogic.js" as MovesLogic
import QtQuick.Dialogs 1.0
Rectangle {

    id: gameScreen
    color: "#222222"
    anchors.fill: parent

    MouseArea{
        anchors.fill: parent
    }
    Component.onCompleted: {
        TurnManager.clearHistory();
    }
    function gameOver(){
        turnIdent.text="GAME OVER!!!";
    }
    function statusChanged(status){
        gameStatus.text=status;
    }
    Column{
        anchors.fill: parent
        spacing: 10
        Item{
            id: gameBoard
            width: parent.width
            height: parent.width
            Component.onCompleted: {
                var obj=Utils.createScreen( Widgets ,"ChessBoard", gameBoard )
                obj.gameOvered.connect(gameOver);
                obj.boardStatus.connect(statusChanged)

            }
        }
        Row{
            width: parent.width
            spacing: 30
            Text {
                id: turnIdent
                text: "Turn: white"
                color: "white"
                Connections {
                    target: TurnManager
                    onTurnChanged: {
                        turnIdent.text=turnIdent.text.split(":")[0]+": " +turnVal;
                    }
                }

            }
            Text {
                id: gameStatus
                text: ""
                color: "white"
            }
        }
        Button{
            id: buttonSave
            width:  parent.width
            height: 30
            text: "Save game"
            onClicked: {

                var obj=Utils.createScreen( Widgets, "SaveDialog",gameScreen);
            }
            style: ButtonStyle {
                label: Text {
                    text: buttonSave.text
                    color: "black"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 14
                }
            }
        }

        Button{
            id: buttonBack
            width:  parent.width
            height: 30
            text: "Stop game"
            onClicked: {
                Utils.moveToMenu(gameScreen);
            }
            style: ButtonStyle {
                label: Text {
                    text: buttonBack.text
                    color: "black"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 14
                }
            }
        }


    }//Collum

}

