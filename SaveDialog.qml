import QtQuick 2.2
import QtQuick.Dialogs 1.0

FileDialog {

    id: fileDialog
    title: "Please choose a file"
    nameFilters: [ "Chess game files (*.chf)"]
    selectExisting: false
    signal urlReady( var url )
    onAccepted: {
        console.log("You chose: " + fileDialog.fileUrl )
        TurnManager.saveCurrentHistory(fileDialog.fileUrl);
    }
    onRejected: {
        console.log("Canceled")

    }
    Component.onCompleted: visible = true
}
