Qt.include("qrc:/Units/HorseBehavior.js")
Qt.include("qrc:/Units/KingBehavior.js")
Qt.include("qrc:/Units/OfficerBehavior.js")
Qt.include("qrc:/Units/PawnBehavior.js")
Qt.include("qrc:/Units/QueenBehavior.js")
Qt.include("qrc:/Units/TurretBehavior.js")

function unitBehavior(){
    return {
        pawn    : PawnBehavior()   ,
        horse   : HorseBehavior()  ,
        officer : OfficerBehavior(),
        king    : KingBehavior()   ,
        turret  : TurretBehavior() ,
        queen   : QueenBehavior()
    }
}
function filter(model,callback){

    var result=[];
    if(!model)
        return result;
    var maxCount=model.count ? model.count :model.length;
    for(var i=0,j=0;i<maxCount;i++){
        if(callback(model.get(i),i)){
            result[j++]=model.get(i);
        }
    }
    return result;
}
function reduce(model,initVal,callback){
    if(!model)
        return initVal;
    var result=initVal;
    var maxCount=model.count ? model.count :model.length;
    //console.log("reduce=","maxCount=",maxCount )
    for(var i=0,j=0;i<maxCount;i++){
        result=callback( model.count ? model.get(i) : model[i],result);
    }
    return result;
}
function isFieldUnderAttack(model,currentColor,fieldNumber){
    var oppositeForces=filter(model,function(item,index){
        if( item.iColor!=currentColor){
            item.tmpIndex=index;
            return true;
        }
        return false;
    });
    var bechavior=unitBehavior();
    console.log("isFieldUnderAttac","oppositeForces=",oppositeForces.length);
    return reduce(oppositeForces,"no",function(item,result){
        if(result==="yes")
            return result;
        var be=bechavior[item.iName];

        if( be ){
            var figureAction=be.isAccesebleMove(model, item.tmpIndex,fieldNumber);
            if(figureAction==="yes" || figureAction ==="eatable" || figureAction==="pawnTransform"){
                console.log("YES MATCHED=",item.iColor,item.iName,item.tmpIndex,fieldNumber);
                return "yes";
            }
        }
        console.log("NO matched=",item.iColor,item.iName,item.tmpIndex,fieldNumber);
        return "no";
    });

}
function getAllKings(model){
    return filter(model,function(item,index){
        if( item.iName==="king"){
            item.tmpIndex=index;
            return true;
        }
        return false;
    });
}
function getAllKingsUnderAttack(model){
    var kings=getAllKings(model);

    var result=[];
    for(var i=0,j=0;i<kings.length ;i ++){
        //        console.log("getAllKingsUnderAttack","kings[i].iColor=",kings[i].iColor)
        //        console.log("getAllKingsUnderAttack","kings[i].tmpIndex=",kings[i].tmpIndex)
        var attacked=isFieldUnderAttack(model,kings[i].iColor,kings[i].tmpIndex);



        if(attacked==="yes"){
            kings[i].attacked=true;
            result[j++]=kings[i];

        }
    }
    return result;
}
