
Qt.include("qrc:/Units/BehaviorPrtotype.js")

var PawnBehavior = function ( ) {
    var that = BehaviorPrototype( );
    that.isAccesebleMove = function(model ,  oldIndex , newIndex){
        console.log("PawnBehavior.isAccesebleMove",model,oldIndex,newIndex)   ;
        if( !model || oldIndex<0 || newIndex<0 )
            return;
        var currentElement=model.get(oldIndex);
        var oldRowNumber=parseInt(oldIndex/8);
        var newRowNumber=parseInt(newIndex/8);

        if(    ( newIndex===oldIndex-8 && currentElement.iColor==="white" )
                || ( newIndex===oldIndex+8 && currentElement.iColor==="black" )
                || ( currentElement.iTouched===false && newIndex===oldIndex+16 && currentElement.iColor==="black" )
                || ( currentElement.iTouched===false && newIndex===oldIndex-16 && currentElement.iColor==="white" )
                ){
             console.log("PawnBehavior.CandidateToYes",that.isElementIsEmpy(model,newIndex),that.isEatable(model,oldIndex,newIndex));
            if(  that.isElementIsEmpy(model,newIndex)  )
            {
                if(    newRowNumber===0 && currentElement.iColor==="white"
                    || newRowNumber===7 && currentElement.iColor==="black"){
                    return "pawnTransform";
                }

                return "yes";
            }
        }

        if(    !that.isElementIsEmpy(model,newIndex)
                && that.isEatable(model,oldIndex,newIndex)
                && (   ( newIndex===oldIndex-8+1 && currentElement.iColor==="white" )
                    || ( newIndex===oldIndex-8-1 && currentElement.iColor==="white" )
                    || ( newIndex===oldIndex+8+1 && currentElement.iColor==="black" )
                    || ( newIndex===oldIndex+8-1 && currentElement.iColor==="black" )
                    )
                ){

            if(    newRowNumber===0 && currentElement.iColor==="white"
                || newRowNumber===7 && currentElement.iColor==="black"){
                return "pawnTransform";
            }
            return "eatable";
        }


        return "no";
    }
    return that;
}
