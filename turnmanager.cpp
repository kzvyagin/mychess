#include "turnmanager.h"
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
#include <QObject>
#include <QJsonValue>
#include <QUrl>
#include <QByteArray>

TurnManager::TurnManager(QObject *parent) : QObject(parent)
{

}

void TurnManager::turnHappened(QVariant event,
                               QVariantMap item,
                               QVariant newPos,
                               QVariant fromPos,
                               QVariantMap lendItem)
{

    //    qDebug()<<Q_FUNC_INFO<<item;
    //    qDebug()<<Q_FUNC_INFO<<event;
    //    qDebug()<<Q_FUNC_INFO<<fromPos<<newPos;
    //    qDebug()<<Q_FUNC_INFO<<"lendItem="<<lendItem;
    QJsonObject turn;
    turn.insert("color",item.value("iColor").toString());
    turn.insert("event",event.toString());
    turn.insert("name",item.value("iName").toString());
    turn.insert("to",newPos.toString());
    turn.insert("from",fromPos.toString());
    turn.insert("eated figure name",lendItem.value("iName").toString());
    turn.insert("eated figure color",lendItem.value("iColor").toString());
    TurnsStory::Instance().saveTurn(turn);
    emit turnChanged(whoseMove());
}

QString TurnManager::whoseMove()
{
    const QJsonArray &story=TurnsStory::Instance().getHistory();
    if(story.size()==0)
        return "white";
    //qDebug()<<Q_FUNC_INFO<<story.at(story.size()-1).toObject().value("color");
    QString lastColor=story.at(story.size()-1).toObject().value("color").toString();
    if(lastColor=="white")
        return "black";
    return "white";

}

void TurnManager::saveCurrentHistory(QVariant fileName)
{
    QUrl name=fileName.toUrl();
    QString filePath=name.path();
    if(!filePath.endsWith(".chf"))
        filePath+=".chf";

    QFile saveFile(filePath);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return ;
    }

    QJsonArray gameObject=TurnsStory::Instance().getHistory();

    QJsonDocument saveDoc(gameObject);
    saveFile.write(  saveDoc.toJson() );
    return;
}

void TurnManager::loadHistory(QVariant fileName)
{
    QUrl name=fileName.toUrl();
    QString filePath=name.path();
    if(!filePath.endsWith(".chf"))
        filePath+=".chf";

    QFile loadFile(filePath);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return ;
    }
    QByteArray saveData = loadFile.readAll();


    QJsonDocument saveDoc= QJsonDocument::fromJson(saveData);
    TurnsStory::Instance().setHistory( saveDoc.array() );

    return;
}

void TurnManager::clearHistory( )
{
    TurnsStory::Instance().getHistory()=QJsonArray();
}

int TurnManager::getToltalTurnsCount()
{
    return TurnsStory::Instance().getHistory().size();
}

QVariantMap TurnManager::getTurn(QVariant _number)
{
    QVariantMap map;
    map.insert("from",0);
    map.insert("to",0);
    int number=_number.toInt();
    //qDebug()<<_number<<number;
    QJsonArray & history=TurnsStory::Instance().getHistory();
    if(number<0 || number>=history.size())
        return map;
    QJsonObject obj=history.at(number).toObject();
    //qDebug()<<Q_FUNC_INFO<<obj;
    int from =obj.value("from").toString().toInt();
    int to=obj.value("to").toString().toInt();

    QString name  =obj.value("eated figure name").toString();
    QString color =obj.value("eated figure color").toString();
    QString event =obj.value("event").toString();
    map["from"]=from;
    map["to"]=to;
    map["ename"]=name;
    map["ecolor"]=color;
    map["event"]=event;
    return map;
}


