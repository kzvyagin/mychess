Qt.include("qrc:/Units/BehaviorPrtotype.js")
var TurretBehavior = function ( ) {
    var that = BehaviorPrototype( );
    that.isAccesebleMove = function(model ,  oldIndex , newIndex){
        console.log("TurretBehavior.isAccesebleMove",model,oldIndex,newIndex)   ;
        var stright=that.isStrightLine(model,oldIndex,newIndex);
        console.log( "TurretBehavior.isStrightLine=", stright);

        if( that.isCastlingAvailible(model ,  oldIndex , newIndex) ){
            return "castling"
        }

        if(stright)
        {
            var bloked=that.isStrightLineBlocked(model,oldIndex,newIndex)
            if(bloked){
                return "no"
            }

            if(that.isElementIsEmpy(model,newIndex) ){
                return "yes"
            }

            if(    !that.isElementIsEmpy(model,newIndex)
                && that.isEatable(model,oldIndex,newIndex) ){
                return "eatable";
            }
        }
        return "no";
    }
    return that;
}
