import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "MyLib.js" as MyLib
import "CommonWidgets.js" as Widgets
import "Utils.js" as Utils
import QtQuick.Dialogs 1.0
Rectangle {

    id: loadScreen
    anchors.fill: parent
    color: "green"
    MouseArea{
        anchors.fill: parent
    }

    Component.onCompleted: {
        TurnManager.clearHistory( );
        turnsCounterText.text="0 of 0";
    }
    property int currentTurnNumber: 0

    onCurrentTurnNumberChanged: {
        turnsCounterText.text=loadScreen.currentTurnNumber+" of "+ TurnManager.getToltalTurnsCount();
    }
    function statusChanged(status){
        gameStatus.text=status;
    }
    function loadCompliteSuccesfully(){

        loadScreen.currentTurnNumber=0;
        gameBoard.children[0].fillDataToModelByDefault();
        turnsCounterText.text=loadScreen.currentTurnNumber+" of "+ TurnManager.getToltalTurnsCount();

    }


    Column{
        id: buttonsRow
        anchors.fill :parent
        spacing:  5
        Button{

            width:  parent.width
            height: 35
            text: "Load"
            onClicked: {
                var obj=Utils.createScreen( Widgets ,"LoadDialog", gameBoard )
                obj.userSelectFile.connect(loadCompliteSuccesfully) ;
            }

        }

        Item{
            id: gameBoard
            width: parent.width
            height: parent.width
            Component.onCompleted: {
                var obj=Utils.createScreen( Widgets ,"ChessBoard", gameBoard )
                obj.chessReadOnly=true
                obj.boardStatus.connect(statusChanged)
            }
        }

        Row{
            height: 35
            width: parent.width
            spacing: 20
            Button{

                width:  70
                height: 35
                text: "Prev"
                onClicked: {
                    if(loadScreen.currentTurnNumber-1 < 0)
                        return;
                    loadScreen.currentTurnNumber-=1;

                    var turnVal=TurnManager.getTurn(loadScreen.currentTurnNumber);
                    console.log("Prev pressed",loadScreen.currentTurnNumber,"turnVal=",JSON.stringify(turnVal));

                    gameBoard.children[0].makeReverceMove(turnVal)

                }

            }
            Text {

                text: "Turn:";
            }
            Text {
                id: turnsCounterText
                text: "0 of 0";
            }
            Button{

                width:  70
                height: 35
                text: "Next"
                onClicked: {
                    var turnVal=TurnManager.getTurn(loadScreen.currentTurnNumber);
                    console.log(JSON.stringify(turnVal));
                    gameBoard.children[0].makeMove(turnVal.from,turnVal.to);
                    var maxTurn=TurnManager.getToltalTurnsCount();
                    if(loadScreen.currentTurnNumber<maxTurn)
                        loadScreen.currentTurnNumber+=1;
                }

            }
            Text {
                id: gameStatus
                text: "";
            }
        }

        Button{

            width:  parent.width
            height: 35
            text: "Start"
            onClicked: {
                Utils.moveToMenu(loadScreen);
                MyLib.rootRef.createGameField();
            }

        }


        Button{

            width:  parent.width
            height: 35
            text: "Back to menu"
            onClicked: { Utils.moveToMenu(loadScreen); }

        }
    }

}
//Start button – starts a new game. Leads to the screen_2

//o Load button – allows user to load saved game. Leads to the screen_3

//o Buttons “prev” and “next”

//o chess board with pieces
