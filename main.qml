import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "Utils.js" as Utils
import "CommonWidgets.js" as Widgets
import "MyLib.js" as MyLib
Window {
    visible: true
    width: 480
    height: 640
    color: "#581258"
    id:desctopItem
    Component.onCompleted: {
        MyLib.rootRef=desctopItem;
    }

    Item{
        id: menuItem
        objectName : "MenuItem"
        anchors.fill: parent


        Item{
            id: buttonsArea
            property int numberOfButtons: 5
            width: parent.width
            anchors{
                right: parent.right
                rightMargin: 5
                left: parent.left
                leftMargin: 5
                top: parent.top
                bottom: parent.bottom
            }
            Column{
                id: myCol
                anchors.centerIn: parent
                spacing: 10
                Button {

                    width:  buttonsArea.width-5
                    height: (buttonsArea.height/buttonsArea.numberOfButtons)-parent.spacing
                    text:  qsTr("Start");
                    onClicked: {
                        createGameField();

                    }
                }

                Button {

                    width:  buttonsArea.width-5
                    height: (buttonsArea.height/buttonsArea.numberOfButtons)-parent.spacing
                    text:  qsTr("Load");
                    onClicked: Utils.createScreen( Widgets ,"LoadScreen", desctopItem )
                }
                Button {

                    width:  buttonsArea.width-5
                    height: (buttonsArea.height/buttonsArea.numberOfButtons)-parent.spacing
                    text:  qsTr("Empty chess board");
                    onClicked: { console.log("I did'n know by what reason this button is here :). But in task it exist. And  I put it here.") }
                }
                Button {

                    width:  buttonsArea.width-5
                    height: (buttonsArea.height/buttonsArea.numberOfButtons)-parent.spacing
                    text:  qsTr("About");
                    onClicked: {Utils.createScreen( Widgets ,"AboutScreen", desctopItem ); menuItem.visible=false}
                }
                Button{
                    width:  buttonsArea.width-5
                    height: (buttonsArea.height/buttonsArea.numberOfButtons)-parent.spacing
                    text:  qsTr("Exit");
                    onClicked: Qt.quit()
                }


            }// Column



        }


    }

    function createGameField(){
        Utils.createScreen( Widgets , "GameScreen" ,desctopItem );
    }
}
