#include <QQmlContext>
#include "utils.h"
#include "chessunitprovider.h"
#include "turnsstory.h"
#include "refbook.h"
#include "turnmanager.h"

Utils::Utils()
{

}

void Utils::registerPixmapProvider(QQmlApplicationEngine *viewer)
{
    viewer->addImageProvider( "ChessUnit" , new  ChessUnitProvider()  );

}


void Utils::registerStuffObjects(QQmlApplicationEngine *viewer)
{
    TurnsStory &sm=TurnsStory::Instance();
    viewer->rootContext()->setContextProperty( "StoryMoves",  &sm);
    static RefBook refbook;
    viewer->rootContext()->setContextProperty( "RefBook",  &refbook);

    static TurnManager turnMagager;
    viewer->rootContext()->setContextProperty( "TurnManager",  &turnMagager);
}

