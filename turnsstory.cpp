#include "turnsstory.h"
#include <QDebug>
TurnsStory::TurnsStory(QObject *parent) : QObject(parent)
{

}

TurnsStory &TurnsStory::Instance()
{
    static TurnsStory story;
    return story;
}

void TurnsStory::saveTurn(QJsonObject turn)
{
    history.append(turn);
    //qDebug()<<Q_FUNC_INFO<<history;
}


QJsonArray &TurnsStory::getHistory()
{
    return history;
}

void TurnsStory::setHistory(const QJsonArray &value)
{
    history = value;
    //qDebug()<<Q_FUNC_INFO<<history;
}


