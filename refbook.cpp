#include "refbook.h"
#include <QDebug>
RefBook::RefBook(QObject *parent) : QObject(parent)
{

}

QStringList RefBook::getFigureNameFromStartSet(QVariant index)
{
    int i=index.toInt();
    return QStringList()<<getStartSet().value(i,"").split(" ");
}

const QHash<int,QString> &RefBook::getStartSet()
{
    static QHash<int,QString> startSet;
    if(startSet.isEmpty()){
        startSet.insert(0,"turret black");
        startSet.insert(1,"horse black");
        startSet.insert(2,"officer black");
        startSet.insert(3,"king black");
        startSet.insert(4,"queen black");
        startSet.insert(5,"officer black");
        startSet.insert(6,"horse black");
        startSet.insert(7,"turret black");


        for(int i=8;i<16;i++)
            startSet.insert(i,"pawn black");

        for(int i=48;i<56;i++)
            startSet.insert(i,"pawn white");

        startSet.insert(56,"turret white");
        startSet.insert(57,"horse white");
        startSet.insert(58,"officer white");
        startSet.insert(59,"king white");
        startSet.insert(60,"queen white");
        startSet.insert(61,"officer white");
        startSet.insert(62,"horse white");
        startSet.insert(63,"turret white");
        qDebug()<<Q_FUNC_INFO<<startSet.size()<<startSet;
    }
    return startSet;
}


