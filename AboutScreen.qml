import QtQuick 2.0

import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0

import "Utils.js" as Utils
Rectangle {
    id: aboutScreen
    anchors.fill: parent
    color: "#AA0000AA"
    MouseArea{
        anchors.fill: parent

    }
    property int scaledFontSize: 14
    Column{
        id: propCollomn
        anchors.left: aboutScreen.left
        anchors.leftMargin: aboutScreen.width/10
        anchors.right: aboutScreen.right
        anchors.rightMargin: aboutScreen.width/10
        anchors.top:  aboutScreen.top
        anchors.topMargin: aboutScreen.height/10
        spacing: 10
        Label{
            width: propCollomn.width
            text: "Application Autor: Konstantin Zvyagin \ne-mail: konstantin.zvyag@gmail.com\nor 4250046@gmail.com\nPhone numer: +79312075129"
            color: "yellow"
            font.pixelSize: aboutScreen.scaledFontSize
        }


        Button{
            id: buttonBack
            height: 70
            text: "Back to menu"
            onClicked: {Utils.moveToMenu(aboutScreen);}
            style: ButtonStyle {
                label: Text {
                    text: buttonBack.text
                    color: "black"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: aboutScreen.scaledFontSize
                }
            }
        }
    }


}
