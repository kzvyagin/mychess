#ifndef TURNSSTORY_H
#define TURNSSTORY_H

#include <QObject>
#include <QVariant>
#include <QJsonArray>
#include <QJsonObject>

class TurnsStory : public QObject
{
    Q_OBJECT
    TurnsStory( QObject *parent = 0 );
    TurnsStory( const TurnsStory& );
    TurnsStory& operator= ( const TurnsStory& );
public:
    static TurnsStory & Instance();

    QJsonArray & getHistory()  ;
    void setHistory(const QJsonArray &value);

signals:

public slots:
    void saveTurn(QJsonObject turn);

private:
    QJsonArray history;
};

#endif // TURNSSTORY_H
