#include "chessunitprovider.h"

ChessUnitProvider::ChessUnitProvider(): QQuickImageProvider(QQuickImageProvider::Pixmap),
    totalUnitsCount(6),
    totalRows(2),
    whiteRow(1),
    blackRow(0)
{
    puzzleImage.load("://images/images/chess-figures.png");
    oneUnitSize=QSize(puzzleImage.size().width() /totalUnitsCount,puzzleImage.size().height()/totalRows);

    unitsMap.insert("turret black", QRect(QPoint(0,0),oneUnitSize));
    unitsMap.insert("officer black",QRect(QPoint(oneUnitSize.width()  ,0),oneUnitSize));
    unitsMap.insert("king black",   QRect(QPoint(oneUnitSize.width()*2,0),oneUnitSize));
    unitsMap.insert("queen black",  QRect(QPoint(oneUnitSize.width()*3,0),oneUnitSize));
    unitsMap.insert("horse black",  QRect(QPoint(oneUnitSize.width()*4,0),oneUnitSize));
    unitsMap.insert("pawn black",   QRect(QPoint(oneUnitSize.width()*5,0),oneUnitSize));
    unitsMap.insert("turret white", QRect(QPoint(0,oneUnitSize.height()),oneUnitSize));
    unitsMap.insert("officer white",QRect(QPoint(oneUnitSize.width()  ,oneUnitSize.height()),oneUnitSize));
    unitsMap.insert("king white",   QRect(QPoint(oneUnitSize.width()*2,oneUnitSize.height()),oneUnitSize));
    unitsMap.insert("queen white",  QRect(QPoint(oneUnitSize.width()*3,oneUnitSize.height()),oneUnitSize));
    unitsMap.insert("horse white",  QRect(QPoint(oneUnitSize.width()*4,oneUnitSize.height()),oneUnitSize));
    unitsMap.insert("pawn white",   QRect(QPoint(oneUnitSize.width()*5,oneUnitSize.height()),oneUnitSize));
}

QPixmap ChessUnitProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(size);
    Q_UNUSED(requestedSize);

    //qDebug()<<Q_FUNC_INFO<<__LINE__<<"id="<<id<<"Size="<<*size<<"requestedSize="<<requestedSize;
    QPixmap px(100,100);
    px.fill(QColor("red"));
    QStringList lst=id.split("=",QString::SkipEmptyParts);

    if(lst.size()!=2){
        qDebug()<<Q_FUNC_INFO<<__LINE__<<"NO PICTURE";
        QPixmap px(100,100);
        px.fill(QColor("red"));
        return px;
    }
    //qDebug()<<lst;

    QRect rectangle=unitsMap.value( lst.value(1) );
    if( rectangle.isNull() ){
        qDebug()<<Q_FUNC_INFO<<__LINE__<<"RECTANLGE IS NULL";
        return puzzleImage.copy( QRect(QPoint(0,0),oneUnitSize) );
    }
    return puzzleImage.copy( rectangle );

}
